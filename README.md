# README #

This set of scripts is to create a web API in JSON from a Raspberry Pi that is mounted with a BME280 temp/pressure/humidity sensor.
A simple visit of the pi's IP would reveal the data in the form of a JSON API.


### How do I get set up? ###

* Configuration

### Configuration
First, you will need to install some dependencies for the BME280 libraries. This will be compatible on DEB-alike systems.
```
sudo apt-get update 
sudo apt-get install apache2 php-common php-cli build-essential python-pip python-dev python-smbus git
```

Navigate to the Adafruit_Python_GPIO directory and start the install
```
cd Adafruit_Python_GPIO 
sudo python setup.py install
```

Once that is installed you will want to place the sensor folder somewhere you might like. In my case I added an user on the system and I am using it's home directory: /home/sensor
```
adduser sensor
mv sensor/* /home/sensor/
```

You also want to put api.php inside the Apache DocumentRoot folder
```
mv api.php /var/www/api.php
```

Start apache and make it start with the system
```
service apache2 start
update-rc.d apache2 defaults
```

By visiting your Raspberry Pi URL you should now be able to view the sensor's data.