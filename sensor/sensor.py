from Adafruit_BME280 import *

sensor = BME280(mode=BME280_OSAMPLE_8)

degrees = sensor.read_temperature()
pascals = sensor.read_pressure()
hectopascals = pascals / 100
humidity = sensor.read_humidity()

print 'timestamp={0:0.3f}'.format(sensor.t_fine)
print 'temp={0:0.3f}'.format(degrees)
print 'pressure={0:0.2f}'.format(hectopascals)
print 'humidity={0:0.2f}'.format(humidity)
